/* eslint-disable no-unused-expressions */
/* eslint-disable no-unused-vars */

import chai from 'chai'
import { Cryptos } from '../src/cryptos'
const should = chai.should()

describe('Crypto', () => {
  it('should get a list of crypto-currencies', () => {
    const c = new Cryptos()
    const res = c.list()
    res.should.be.an('array')
    res.length.should.above(0)
  })
})
