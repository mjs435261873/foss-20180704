import 'babel-core/register'
import 'babel-polyfill'
import pkg from '../package.json'

console.log(`You are running ${pkg.name} version ${pkg.version}`)
